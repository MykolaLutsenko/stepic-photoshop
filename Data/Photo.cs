using System;
using MyPhotoshop.Data;

namespace MyPhotoshop
{
	public class Photo
	{
		public readonly int width;
		public readonly int height;
		private readonly Pixel[,] pixelData;

        public Photo()
        {

        }

        public Photo(int width, int height)
        {
            this.width = width;
            this.height = height;
            pixelData = new Pixel[width,height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    pixelData[x,y] = new Pixel();
                }
            }
        }

        public Pixel this[int width, int height]
        {
            get {return pixelData[width,height];}
            //������ �� �����
            set { pixelData[width,height] = value; }
        }
    }

}

