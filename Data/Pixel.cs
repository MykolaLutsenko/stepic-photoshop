﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPhotoshop.Data
{
    public struct Pixel
    {
        private int x;
        private int y;
        private double r;
        private double g;
        private double b;

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set {y = value; }
        }
        public double R
        {
            get { return r; }
            set
            {
                r = CheckAndTrim(value);
            }
        }
        public double G
        {
            get { return g; }
            set
            {
                g = CheckAndTrim(value);
            }
        }
        public double B
        {
            get { return b; }
            set
            {
                b = CheckAndTrim(value);
            }
        }

        public Pixel(int _x, int _y, double _R, double _G, double _B)
        {
            this.x = this.y = 0;
            this.r = this.g = this.b = 0d;
            //у структур и свойст странные взаимоотношения. нельзя вот так просто взять и присвоить в конструкторе
            this.X = _x;
            this.Y = _y;
            this.R = _R;
            this.G = _G;
            this.B = _B;
        }
        
       public static Pixel operator *(Pixel p1, Pixel p2)
       {
           Pixel result = new Pixel();
           result.R = p1.R * p2.R;
           result.G = p1.G * p2.G;
           result.B = p1.B * p2.B;

           return result;

       }

       public static Pixel operator *(Pixel p, double d)
       {
           Pixel result = new Pixel();
           result.R = p.R * d;
           result.G = p.G * d;
           result.B = p.B * d;

           return result;

       }
        //нужно чтобы операции были комутативны
        public static Pixel operator *(double d, Pixel p)
        {
            //вернем рекурсивный вызов предыдущего оператора
            return p * d;

        }

        //избегаем повторения одного и того же кода. всегда выносим его в отдельную функциональность которую вызываем
        public double CheckAndTrim(double value)
       {
           if (value < 0 || value > 1) Trim(value);
           return value;
       }

       public static Pixel Trim(Pixel pixel)
       {
           if (pixel.R > 1) pixel.R = 1;
           if (pixel.R < 0) pixel.R = 0;
           if (pixel.G > 1) pixel.G = 1;
           if (pixel.G < 0) pixel.G = 0;
           if (pixel.B > 1) pixel.B = 1;
           if (pixel.B < 0) pixel.B = 0;

           return pixel;
       }

       public static double Trim(double value)
       {
           if (value < 0) return 0;
           if (value > 1) return 1;

           return value;
       }

    }
}
